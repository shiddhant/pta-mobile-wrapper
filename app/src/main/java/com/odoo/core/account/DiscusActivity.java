package com.odoo.core.account;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.CalendarView;
import android.widget.Toast;

import com.odoo.R;
import com.odoo.core.utils.OAppBarUtils;

public class DiscusActivity extends AppCompatActivity {
    public static final String TAG = DiscusActivity.class.getSimpleName();
    CalendarView mCalender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discus);

        OAppBarUtils.setAppBar(this, true);
        ActionBar actionbar = getSupportActionBar();
        if(actionbar!=null) {
            actionbar.setHomeButtonEnabled(true);
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setTitle("Discuss");
        }


//        mCalender = (CalendarView)findViewById(R.id.calender);
//        mCalender.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
//            @Override
//            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
//                Toast.makeText(DiscusActivity.this, "date:"+i2+"/"+i1+"/"+i, Toast.LENGTH_SHORT).show();
//            }
//        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}


