package com.odoo.core.account;


import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.odoo.R;
import com.odoo.core.utils.OAppBarUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CalendarActivity extends AppCompatActivity {
    public static final String TAG = CalendarActivity.class.getSimpleName();



    CompactCalendarView compactCalendarView;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-yyyy", Locale.getDefault());
    private ViewPager mViewPager;
    private TabLayout mTabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        OAppBarUtils.setAppBar(this, true);
        ActionBar actionbar = getSupportActionBar();
        if(actionbar!=null) {
            actionbar.setHomeButtonEnabled(true);
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setTitle(R.string.title_calendar_events);
        }


        compactCalendarView = (CompactCalendarView) findViewById(R.id.compactcalendar_view);
        // Set first day of week to Monday, defaults to Monday so calling setFirstDayOfWeek is not necessary
        // Use constants provided by Java Calendar class
        compactCalendarView.setFirstDayOfWeek(Calendar.MONDAY);

        // Add event 1 on Sun, 07 Jun 2015 18:20:51 GMT
        Event ev1 = new Event(Color.GREEN, 1433701251000L, "Some extra data that I want to store.");
        compactCalendarView.addEvent(ev1);

        // Added event 2 GMT: Sun, 07 Jun 2015 19:10:51 GMT
        Event ev2 = new Event(Color.GREEN, 1433704251000L);
        compactCalendarView.addEvent(ev2);

        // Query for events on Sun, 07 Jun 2015 GMT.
        // Time is not relevant when querying for events, since events are returned by day.
        // So you can pass in any arbitary DateTime and you will receive all events for that day.
        List<Event> events = compactCalendarView.getEvents(1433701251000L); // can also take a Date object

        // events has size 2 with the 2 events inserted previously
        //Log.d(TAG, "Events: " + events);

        // define a listener to receive callbacks when certain events happen.
        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {

                List<Event> events = compactCalendarView.getEvents(dateClicked);
                Toast.makeText(CalendarActivity.this, "Day was clicked: " + dateClicked + " with events " + events, Toast.LENGTH_SHORT).show();
                // Log.d(TAG, "Day was clicked: " + dateClicked + " with events " + events);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                Toast.makeText(CalendarActivity.this, "Month was scrolled to: " + firstDayOfNewMonth, Toast.LENGTH_SHORT).show();
                //Log.d(TAG, "Month was scrolled to: " + firstDayOfNewMonth);
            }
        });



        mViewPager = (ViewPager) findViewById(R.id.content_drawer_viewpager);
        setUpViewPager(mViewPager);
        mTabLayout = (TabLayout) findViewById(R.id.content_drawer_tabs);
        if (mTabLayout != null) {
            mTabLayout.setupWithViewPager(mViewPager);
        }

    }

    /**
     * @param viewPager
     * @user-define method - used to add the fragment in the adapter.
     */
    private void setUpViewPager(ViewPager viewPager) {

        myPagerAdapter myPagerAdapter1 = new myPagerAdapter(getSupportFragmentManager());
       myPagerAdapter1.addFragment(new CalendarEventsFrag(), "Events");
        viewPager.setAdapter(myPagerAdapter1);
    }

    /**
     * @class
     */
    private class myPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        /**
         * @param fm
         * @constructor
         */
        public myPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        /**
         * @param position
         * @return
         * @Override method - return the fragment.
         */
        @Override
        public Fragment getItem(int position) {

            return mFragmentList.get(position);
        }

        /**
         * @return
         * @Override method - return the total no. of fragment.
         */
        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        /**
         * @param fragment
         * @param title
         * @user-define method - used to add the fragment.
         */
        private void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        /**
         * @param position
         * @return
         * @override method - used to get page title.
         */
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }


    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}


