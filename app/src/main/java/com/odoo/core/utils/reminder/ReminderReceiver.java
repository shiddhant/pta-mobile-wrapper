/**
 * Odoo, Open Source Management Solution
 * Copyright (C) 2012-today Odoo SA (<http:www.odoo.com>)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http:www.gnu.org/licenses/>
 *
 * Created on 9/1/15 6:15 PM
 */
package com.odoo.core.utils.reminder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.odoo.R;

import com.odoo.base.addons.res.ResPartner;
import com.odoo.core.orm.ODataRow;
import com.odoo.core.orm.fields.OColumn;
import com.odoo.core.utils.OResource;
import com.odoo.core.utils.notification.ONotificationBuilder;

public class ReminderReceiver extends BroadcastReceiver {
    public static final String TAG = ReminderReceiver.class.getSimpleName();

   /* public static final int REQUEST_EVENT_REMINDER = 12345;
    public static final int REQUEST_PHONE_CALL_REMINDER = 12346;
    public static final String ACTION_EVENT_REMINDER_DONE = "action_event_reminder_done";
    public static final String ACTION_EVENT_REMINDER_RE_SCHEDULE = "action_event_reminder_re_schedule";
    public static final String ACTION_PHONE_CALL_REMINDER_CALLBACK = "action_phone_call_reminder_callback";
    public static final String ACTION_PHONE_CALL_REMINDER_DONE = "action_phone_call_reminder_done";
    public static final String ACTION_PHONE_CALL_REMINDER_RE_SCHEDULE = "action_phone_call_reminder_re_schedule";*/

    @Override
    public void onReceive(Context context, Intent intent) {
        String type = intent.getStringExtra(ReminderUtils.KEY_REMINDER_TYPE);
        showNotification(context, type, intent.getExtras());
    }

    private void showNotification(Context context, String type, Bundle data) {
    }

}
